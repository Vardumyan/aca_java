package lesson1._roulette.interfaces;

public interface IPlayer {

   boolean hasMoney();

   void playWith(IDealer dealer);

   void debit(int amount);

   void credit(int amount);

   int getCash();
}
