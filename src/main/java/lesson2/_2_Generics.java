package lesson2;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class A<T> {
   private T f1;

   void set(T t) {
      this.f1 = t;
   }

   T get() {
      return this.f1;
   }
}

class B<T> extends A<T> {

}

public class _2_Generics {

   static <A, B, C> void genericMethod(A a, B b, C c) {
      System.out.println(a);
      System.out.println(b);
      System.out.println(c);
   }

   interface I1 {
      void m1();
   }

   interface I2 {
      void m2();
   }


   static <A extends I1> void upperBound(A a) {
      a.m1();
   }

   static <A extends I1 & I2> void multipleUpperBound(A a) {
      a.m1();
      a.m2();
   }

   static void addNumbers(List<? super Integer> list) {
      for (int i = 1; i <= 10; i++) {
         list.add(i);
      }
   }

   static <T extends Comparable<T>> int countGreaterThan(T[] anArray, T elem) {
      int count = 0;
      for (T e : anArray) {
         if (e.compareTo(elem) > 0) {
            ++count;
         }
      }
      return count;
   }

   public static double sumOfList1(List<Number> list) {
      double s = 0.0;
      for (Number n : list)
         s += n.doubleValue();
      return s;
   }

   public static void main(String[] args) {
      A<String> a1 = new A<>();
      a1.set("string");
      System.out.println(a1.get());

      A<Integer> a2 = new A<>();
      a2.set(1);
      System.out.println(a2.get());

      HashMap<String, Integer> map = new HashMap<>();


      List l1 = new ArrayList();
      l1.add("hello");
      l1.add(1);
      l1.add(24.23);

      genericMethod("hello", 1, 1.4);

//      List<? extends A> l2 = new ArrayList<B>();
//      l2 = new ArrayList<A>();
//
//      List<A> l3 = new ArrayList<A>();
//      l3 = new ArrayList<B>();


   }
}
