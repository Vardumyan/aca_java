package lesson3;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static lesson3.Shared.filePath;
import static lesson3.Shared.randomString;

public class _1_CharacterStreams {
   public static void main(String[] args) throws IOException {

      String string = randomString();

      try (FileWriter fw = new FileWriter(filePath, false)) {
         System.out.println("writing : " + string);
         fw.write(string);
      }

      try (FileReader fr = new FileReader(filePath)) {
         System.out.print("reading : ");

         while (true) {
            int read = fr.read();

            if (read == -1) {
               break;
            }

            System.out.print((char) read);

         }
      }


   }
}
