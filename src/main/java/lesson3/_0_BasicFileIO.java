package lesson3;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static lesson3.Shared.*;

public class _0_BasicFileIO {

   public static void main(String[] args) throws IOException {

      deleteFile(filePath);

      try (FileOutputStream fos = new FileOutputStream(filePath)) {
         for (int i = 0; i < 42; i++) {
            log("writing " + i);
            fos.write(i);
         }
      }

      System.out.println();


      try (FileInputStream fis = new FileInputStream(filePath)) {
         while (true) {
            int read = fis.read();
            log("reading " + read);

            if (read == -1) {
               break;
            }
         }
      }
   }
}
